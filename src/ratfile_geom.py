#!/usr/bin/env python3

from rat import rat
import shutil
import numpy as np

shutil.rmtree('logs', True)

if __name__ == '__main__':

    configs = {
            'epochs': [160],
            'log-interval': 1000,
            'lr': [1e-1, 3e-2],
            'lr_decay_steps': [100000],
            'lr_warmup_steps': [500, 2000, 5000],
            'lr_final_factor': [1e-2],
            # 'weight-decay': [0., 1e-9, 1e-8, 1e7, 1e-6, 1e-5, 1e-4],
            'weight-decay': [0., 1e-9, 1e-8, 1e7],
            'em-iters': [2],
            'optimizer': ['ranger'],
            'batch-size': [32],
            'test-batch-size': [64],
            'net-a': [32, 128, 512],
            'net-b': [16],
            'net-c': [16],
            'net-d': [32],
            'geom': [1],
    }

    rat.run_experiment(configs, 'train.py', search_strategy={
        'create': 'sample',
        'queue_size': 3,
        'keep_best': -1,
        'score': 'summary_scalar',
        'args': {
            'score_key': 'test_acc',
            'lower_is_better': False,
            },
        }, step_after=False,
        )

    print('scheduled')
