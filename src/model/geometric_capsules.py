import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import math

class VecToPose(nn.Module):
    """
    # TODO: Investigate if constraining to [0, pi] stabilizes things
    Closed form implementations from:
    https://jinyongjeong.github.io/Download/SE3/jlblanco2010geometry3d_techrep.pdf
    """
    def __init__(self, pseudo=False, v_len=6, mat_sz=4, closed_form=True):
        super().__init__()
        self.v_len = v_len
        self.mat_sz = mat_sz
        self.pseudo = pseudo
        self.closed_form = closed_form
        self.eps = 1e-8
        
        skew = torch.tensor([[1,-1,1],[1,1,-1],[-1,1,1]])
        
        self.register_buffer('skew', skew.reshape(1,1,1,3,3))
        self.register_buffer('eye', torch.eye(3).reshape(1,1,1,3,3))
        self.register_buffer('homog', torch.tensor((0,0,0,1)).reshape(1,1,1,1,4))
        self.register_buffer('pi', torch.tensor([math.pi]))

    
    def vec_hat(self, x, b, n_in, caps):
        # Holder
        base = x.new_zeros((b, n_in, caps, 3, 3))
        
        # Build skew-symmetric rotation matrix
        base[:,:,:,[1,2],[2,1]] = base[:,:,:,[1,2],[2,1]] + x[:,:,:,0] # X
        base[:,:,:,[2,0],[0,2]] = base[:,:,:,[2,0],[0,2]] + x[:,:,:,1] # Y
        base[:,:,:,[1,0],[0,1]] = base[:,:,:,[1,0],[0,1]] + x[:,:,:,2] # Z
        return base * self.skew

    def map_groups(self, theta, omega_hat):
        cos_coef = ((1 - torch.cos(theta)) / theta**2 ) * omega_hat
        sin_coef = (theta - torch.sin(theta)) / theta**3
        sin_coef = sin_coef * torch.matrix_power(omega_hat, 2)
        
        # First three terms
        return self.eye + cos_coef + sin_coef


    def matrix_exp(self, theta, omega_hat):
        """
        Use the closed form matrix exponential for SE(3) as the inbuilt torch.matrix_exp()
        takes far too long

        # TODO: Add more terms to the taylor expansion
        """

        sin_coef = (torch.sin(theta)/ theta ) * omega_hat
        cos_coef = (1 - torch.cos(theta)) / theta**2 
        cos_coef = cos_coef * torch.matrix_power(omega_hat, 2)
        
        return self.eye + sin_coef + cos_coef

    def matrix_log(self, theta, rotation):
        norm = theta / (2 * torch.sin(theta) + self.eps)
        skew = rotation - rotation.transpose(-2,-1)
        return norm * skew

    def exp_map(self, x, channel_dim=-1):
        
        b, n_in, caps, vec, cast = x.shape

        # Divide into rotational and translational elements
        x_translate = x[:,:,:,:3]
        x_rotate = x[:,:,:,3:]

        # Generate the components for the exponential map
        omega_hat = self.vec_hat(x_rotate, b, n_in, caps)
        
        # Remap translations under rotation
        if (not self.pseudo)|(self.closed_form):
            theta = (x_rotate*x_rotate).sum(dim=-2, keepdim=True) + self.eps

        if not self.pseudo:
            v_mat = self.map_groups(theta, omega_hat)
            x_translate = torch.matmul(v_mat, x_translate)

        # Allow switching based on potential GPU speedup
        if self.closed_form:
            omega_hat = self.matrix_exp(theta, omega_hat)
        else:
            omega_hat = torch.matrix_exp(omega_hat)

        # Assemble homogenous pose matrix
        x = torch.cat((omega_hat, x_translate), dim=-1)
        x = torch.cat((x, self.homog.repeat(*x.shape[:3],1,1).contiguous()), dim=-2)
        return x

    def log_map(self, x):
        b, B, C, P_P = x.shape
        x = x.view(b, B, C, self.mat_sz, self.mat_sz)
        
        # Break out rotational and translation elements
        x_rot, x_trans = x[:,:,:,:3,:3], x[:,:,:,:3,3:4]

        # Determine angle of rotation
        trace = torch.diagonal(x_rot, dim1=-2, dim2=-1).sum(-1, keepdim=True)
        trace = torch.fmod(trace + 1, 8) # Full rotation, as [-1,3] maps to [pi, 0]
        
        theta = torch.acos((torch.fmod(trace, 4) - 2.) / 2.) # Work to limits of arccos
        theta[trace > 4] = theta[trace > 4] - pi # Subtract off remaining rotation
        theta = theta.unsqueeze(-1)

        # Take the matrix log
        omega_hat = self.matrix_log(theta+self.eps, x_rot)
        
        # Map translate back into their natural coords
        if not self.pseudo:
            v_inv = torch.inverse(self.map_groups(theta + self.eps, omega_hat))
            x_trans = torch.matmul(v_inv, x_trans)
        # Assemble output vector
        pos = [omega_hat[:,:,:,2:3,1:2],omega_hat[:,:,:,0:1,2:3],omega_hat[:,:,:,1:2,0:1]] # X, Y, Z
        
        x = torch.cat([x_trans] + pos, dim=-2)
        return x

    def forward(self, x, exp_map=True):
        if exp_map:
            return self.exp_map(x)
        else:
            return self.log_map(x)


class PrimaryCaps(nn.Module):
    r"""Creates a primary convolutional capsule layer
    that outputs a pose matrix and an activation.

    Note that for computation convenience, pose matrix
    are stored in first part while the activations are
    stored in the second part.

    Args:
        A: output of the normal conv layer
        B: number of types of capsules
        K: kernel size of convolution
        P: size of pose matrix is P*P
        stride: stride of convolution
        geom: Whether or not to use the geometric representation
        v_len: number of items in the Lie algebra representation

    Shape:
        input:  (*, A, h, w)
        output: (*, h', w', B*(P*P+1))
        h', w' is computed the same way as convolution layer
        parameter size is: K*K*A*B*P*P + B*P*P

    """
    def __init__(self, A=32, B=32, K=1, P=4, stride=1, geom=False, v_len=6):
        super(PrimaryCaps, self).__init__()
        self.geom = geom
        self.caps_in = A
        self.caps_out = B

        factor = v_len if geom else P*P

        self.pose = nn.Conv2d(in_channels=A, out_channels=B*factor,
                            kernel_size=K, stride=stride, bias=True)
        self.a = nn.Conv2d(in_channels=A, out_channels=B,
                            kernel_size=K, stride=stride, bias=True)
        self.sigmoid = nn.Sigmoid()

        if self.geom:
            self.vtp = VecToPose(A, B)

    def forward(self, x):
        p = self.pose(x).permute(0, 2, 3, 1).contiguous()
        if self.geom:
            b, h, w, c = p.shape
            p = p.reshape(b*h*w, self.caps_out, 1, 6, 1)
            p = self.vtp(p).flatten(-2).reshape(b, h, w, -1)

        a = self.a(x).permute(0, 2, 3, 1).contiguous()
        a = self.sigmoid(a)

        out = torch.cat([p, a], dim=-1)
        return out


class ConvCaps(nn.Module):
    """Create a convolutional capsule layer
    that transfer capsule layer L to capsule layer L+1
    by EM routing.

    Args:
        B: input number of types of capsules
        C: output number on types of capsules
        K: kernel size of convolution
        P: size of pose matrix is P*P
        stride: stride of convolution
        iters: number of EM iterations
        coor_add: use scaled coordinate addition or not
        w_shared: share transformation matrix across w*h.
        geom: Whether or not to use the geometric representation
        v_len: number of items in the Lie algebra representation 

    Shape:
        input:  (*, h,  w, B*(P*P+1))
        output: (*, h', w', C*(P*P+1))
        h', w' is computed the same way as convolution layer
        parameter size is: K*K*B*C*P*P + B*P*P
    """
    def __init__(self, B=32, C=32, K=3, P=4, stride=2, iters=3,
                 coor_add=False, w_shared=False, geom=False,
                 algebraic_attn=False, closed_form=True,
                 v_len=6):
        super(ConvCaps, self).__init__()
        # TODO: lambda scheduler
        # Note that .contiguous() for 3+ dimensional tensors is very slow
        self.B = B
        self.C = C
        self.K = K
        self.P = P
        self.v_len = v_len
        self.geom = geom
        self.algebraic_attn = algebraic_attn
        self.psize = P*P
        self.stride = stride
        self.iters = iters
        self.coor_add = coor_add
        self.w_shared = w_shared
        # constant
        self.eps = 1e-4
        self._lambda = 1e-03
        self.register_buffer('ln_2pi', torch.FloatTensor(1).fill_(math.log(2*math.pi)))
        # params
        # Note that \beta_u and \beta_a are per capsule type,
        # which are stated at https://openreview.net/forum?id=HJWLfGWRb&noteId=rJUY2VdbM
        self.beta_u = nn.Parameter(torch.zeros(C))
        self.beta_a = nn.Parameter(torch.zeros(C))
        # Note that the total number of trainable parameters between
        # two convolutional capsule layer types is 4*4*k*k
        # and for the whole layer is 4*4*k*k*B*C,
        # which are stated at https://openreview.net/forum?id=HJWLfGWRb&noteId=r17t2UIgf
        if geom:
            self.weights = nn.Parameter(torch.randn(1, K*K*B, C, v_len, 1))
            self.vtp = VecToPose(closed_form=closed_form)
        else:
            self.weights = nn.Parameter(torch.randn(1, K*K*B, C, P, P))
        if algebraic_attn:
            self.algattn = VecToPose(closed_form=closed_form)
        # op
        self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax(dim=2)

    def m_step(self, a_in, r, v, eps, b, B, C, psize):
        """
            \mu^h_j = \dfrac{\sum_i r_{ij} V^h_{ij}}{\sum_i r_{ij}}
            (\sigma^h_j)^2 = \dfrac{\sum_i r_{ij} (V^h_{ij} - mu^h_j)^2}{\sum_i r_{ij}}
            cost_h = (\beta_u + log \sigma^h_j) * \sum_i r_{ij}
            a_j = logistic(\lambda * (\beta_a - \sum_h cost_h))

            Input:
                a_in:      (b, C, 1)
                r:         (b, B, C, 1)
                v:         (b, B, C, P*P)
            Local:
                cost_h:    (b, C, P*P)
                r_sum:     (b, C, 1)
            Output:
                a_out:     (b, C, 1)
                mu:        (b, 1, C, P*P)
                sigma_sq:  (b, 1, C, P*P)
        """
        r = r * a_in
        r = r / (r.sum(dim=2, keepdim=True) + eps)
        r_sum = r.sum(dim=1, keepdim=True)
        coeff = r / (r_sum + eps)
        coeff = coeff.view(b, B, C, 1)

        # TODO: Map the attention mechanism into the Lie algebra
        mu = torch.sum(coeff * v, dim=1, keepdim=True)
        sigma_sq = torch.sum(coeff * (v - mu)**2, dim=1, keepdim=True) + eps
        r_sum = r_sum.view(b, C, 1)
        sigma_sq = sigma_sq.view(b, C, psize)
        cost_h = (self.beta_u.view(C, 1) + torch.log(sigma_sq.sqrt())) * r_sum

        a_out = self.sigmoid(self._lambda*(self.beta_a - cost_h.sum(dim=2)))
        sigma_sq = sigma_sq.view(b, 1, C, psize)
        return a_out, mu, sigma_sq

    def e_step(self, mu, sigma_sq, a_out, v, eps, b, C):
        """
            ln_p_j = sum_h \dfrac{(\V^h_{ij} - \mu^h_j)^2}{2 \sigma^h_j}
                    - sum_h ln(\sigma^h_j) - 0.5*\sum_h ln(2*\pi)
            r = softmax(ln(a_j*p_j))
              = softmax(ln(a_j) + ln(p_j))

            Input:
                mu:        (b, 1, C, P*P)
                sigma:     (b, 1, C, P*P)
                a_out:     (b, C, 1)
                v:         (b, B, C, P*P)
            Local:
                ln_p_j_h:  (b, B, C, P*P)
                ln_ap:     (b, B, C, 1)
            Output:
                r:         (b, B, C, 1)
        """
        ln_p_j_h = -1. * (v - mu)**2 / (2 * sigma_sq) \
                    - torch.log(sigma_sq.sqrt()) \
                    - 0.5*self.ln_2pi

        ln_ap = ln_p_j_h.sum(dim=3) + torch.log(a_out.view(b, 1, C))
        r = self.softmax(ln_ap)
        return r

    def caps_em_routing(self, v, a_in, C, eps):
        """
            Input:
                v:         (b, B, C, P*P)
                a_in:      (b, C, 1)
            Output:
                mu:        (b, 1, C, P*P)
                a_out:     (b, C, 1)

            Note that some dimensions are merged
            for computation convenient, that is
            `b == batch_size*oh*ow`,
            `B == self.K*self.K*self.B`,
            `psize == self.P*self.P`
        """
        if self.algebraic_attn:
            v = self.algattn(v, exp_map=False).squeeze(-1)

        b, B, c, psize = v.shape
        assert c == C
        assert (b, B, 1) == a_in.shape

        r = torch.FloatTensor(b, B, C).fill_(1./C).to(v.device)
        for iter_ in range(self.iters):
            a_out, mu, sigma_sq = self.m_step(a_in, r, v, eps, b, B, C, psize)
            if iter_ < self.iters - 1:
                r = self.e_step(mu, sigma_sq, a_out, v, eps, b, C)
        if self.algebraic_attn:
            # Unsqueeze mu for ops, then flatten last dim
            mu = self.algattn(mu.unsqueeze(-1), exp_map=True).flatten(-2)
        
        return mu, a_out

    def add_pathes(self, x, B, K, psize, stride):
        """
            Shape:
                Input:     (b, H, W, B*(P*P+1))
                Output:    (b, H', W', K, K, B*(P*P+1))
        """
        b, h, w, c = x.shape
        assert h == w
        assert c == B*(psize+1)
        oh = ow = int(((h - K )/stride)+ 1) # moein - changed from: oh = ow = int((h - K + 1) / stride)
        idxs = [[(h_idx + k_idx) \
                for k_idx in range(0, K)] \
                for h_idx in range(0, h - K + 1, stride)]
        x = x[:, idxs, :, :]
        x = x[:, :, :, idxs, :]
        x = x.permute(0, 1, 3, 2, 4, 5).contiguous()
        return x, oh, ow

    def transform_view(self, x, w, C, P, w_shared=False):
        """
            For conv_caps:
                Input:     (b*H*W, K*K*B, P*P)
                Output:    (b*H*W, K*K*B, C, P*P)
            For class_caps:
                Input:     (b, H*W*B, P*P)
                Output:    (b, H*W*B, C, P*P)
        """
        b, B, psize = x.shape
        assert psize == P*P
        if self.geom:
            # Transform weights from algebra into group
            w = self.vtp(w)

        x = x.view(b, B, 1, P, P)
        if w_shared:
            hw = int(B / w.size(1))
            w = w.repeat(1, hw, 1, 1, 1)

        w = w.repeat(b, 1, 1, 1, 1)
        x = x.repeat(1, 1, C, 1, 1)
        v = torch.matmul(w, x)
        if self.geom:
            pass
        v = v.view(b, B, C, P*P)
        return v

    def add_coord(self, v, b, h, w, B, C, psize):
        """
            Shape:
                Input:     (b, H*W*B, C, P*P)
                Output:    (b, H*W*B, C, P*P)
        """
        assert h == w
        v = v.view(b, h, w, B, C, psize)
        coor = torch.arange(h, dtype=torch.float32) / h
        coor_h = torch.FloatTensor(1, h, 1, 1, 1, self.psize).fill_(0.).to(v.device)
        coor_w = torch.FloatTensor(1, 1, w, 1, 1, self.psize).fill_(0.).to(v.device)
        coor_h[0, :, 0, 0, 0, 0] = coor
        coor_w[0, 0, :, 0, 0, 1] = coor
        v = v + coor_h + coor_w
        v = v.view(b, h*w*B, C, psize)
        return v

    def forward(self, x):
        
        b, h, w, c = x.shape
        if not self.w_shared:
            # add patches
            x, oh, ow = self.add_pathes(x, self.B, self.K, self.psize, self.stride)
            
            # transform view
            p_in = x[:, :, :, :, :, :self.B*self.psize].contiguous()
            a_in = x[:, :, :, :, :, self.B*self.psize:].contiguous()
            p_in = p_in.view(b*oh*ow, self.K*self.K*self.B, self.psize)
            a_in = a_in.view(b*oh*ow, self.K*self.K*self.B, 1)
            v = self.transform_view(p_in, self.weights, self.C, self.P)
            
            # em_routing
            p_out, a_out = self.caps_em_routing(v, a_in, self.C, self.eps)
            p_out = p_out.view(b, oh, ow, self.C*self.psize)
            a_out = a_out.view(b, oh, ow, self.C)
            out = torch.cat([p_out, a_out], dim=3)
        else:
            assert c == self.B*(self.psize+1)
            assert 1 == self.K
            assert 1 == self.stride
            p_in = x[:, :, :, :self.B*self.psize].contiguous()
            p_in = p_in.view(b, h*w*self.B, self.psize)
            a_in = x[:, :, :, self.B*self.psize:].contiguous()
            a_in = a_in.view(b, h*w*self.B, 1)

            # transform view
            v = self.transform_view(p_in, self.weights, self.C, self.P, self.w_shared)

            # coor_add
            if self.coor_add:
                v = self.add_coord(v, b, h, w, self.B, self.C, self.psize)

            # em_routing
            _, out = self.caps_em_routing(v, a_in, self.C, self.eps)
        
        return out


class CapsNet(nn.Module):
    """A network with one ReLU convolutional layer followed by
    a primary convolutional capsule layer and two more convolutional capsule layers.

    Suppose image shape is 28x28x1, the feature maps change as follows:
    1. ReLU Conv1
        (_, 1, 28, 28) -> 5x5 filters, 32 out channels, stride 2 with padding
        x -> (_, 32, 14, 14)
    2. PrimaryCaps
        (_, 32, 14, 14) -> 1x1 filter, 32 out capsules, stride 1, no padding
        x -> pose: (_, 14, 14, 32x4x4), activation: (_, 14, 14, 32)
    3. ConvCaps1
        (_, 14, 14, 32x(4x4+1)) -> 3x3 filters, 32 out capsules, stride 2, no padding
        x -> pose: (_, 6, 6, 32x4x4), activation: (_, 6, 6, 32)
    4. ConvCaps2
        (_, 6, 6, 32x(4x4+1)) -> 3x3 filters, 32 out capsules, stride 1, no padding
        x -> pose: (_, 4, 4, 32x4x4), activation: (_, 4, 4, 32)
    5. ClassCaps
        (_, 4, 4, 32x(4x4+1)) -> 1x1 conv, 10 out capsules
        x -> pose: (_, 10x4x4), activation: (_, 10)

        Note that ClassCaps only outputs activation for each class

    Args:
        A: output channels of normal conv
        B: output channels of primary caps
        C: output channels of 1st conv caps
        D: output channels of 2nd conv caps
        E: output channels of class caps (i.e. number of classes)
        K: kernel of conv caps
        P: size of square pose matrix
        iters: number of EM iterations
        ...
    """
    def __init__(self, A=32, B=32, C=32, D=32, E=10, K=3, P=4, iters=3,
                    geom=False,algebraic_attn=False, closed_form=True):
        super(CapsNet, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=A,
                               kernel_size=5, stride=2, padding=2)
        self.bn1 = nn.BatchNorm2d(num_features=A, eps=0.001,
                                 momentum=0.1, affine=True)
        self.relu1 = nn.ReLU(inplace=False)
        self.primary_caps = PrimaryCaps(A, B, 1, P, stride=1, geom=geom)
        self.conv_caps1 = ConvCaps(B, C, K, P, stride=2, iters=iters, geom=geom,
                                    algebraic_attn=algebraic_attn,
                                    closed_form=closed_form)
        self.conv_caps2 = ConvCaps(C, D, K, P, stride=1, iters=iters, geom=geom,
                                    algebraic_attn=algebraic_attn,
                                    closed_form=closed_form)
        self.class_caps = ConvCaps(D, E, 1, P, stride=1, iters=iters,
                                    coor_add=True, w_shared=True, geom=geom,
                                    algebraic_attn=algebraic_attn,
                                    closed_form=closed_form)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)
        x = self.primary_caps(x)
        x = self.conv_caps1(x)
        x = self.conv_caps2(x)
        x = self.class_caps(x)
        return x


def capsules(**kwargs):
    """Constructs a CapsNet model.
    """
    model = CapsNet(**kwargs)
    return model


'''
TEST
Run this code with:
```
python -m capsules.py
```
'''
if __name__ == '__main__':
    from math import pi
    bhw = 8*16*16
    kkb = 3*3*16
    c = 32
    n_vec = 6
    scale = 1.

    x = torch.randn(bhw,kkb, c, n_vec, 1, requires_grad=True) * scale
    x = torch.fmod(x, 2)
    vtp = VecToPose()
    vtp(x).mean().backward()
    vtp.zero_grad()
    with torch.no_grad():
        twice = vtp(vtp(x).flatten(-2), exp_map=False)
    print("STABILITY: ", (x - twice).abs().mean().item())
    print("MED: ", (x - twice).abs().median().item())

    x = torch.randn(2,1,32,32, requires_grad=True) * scale

    model = capsules(E=10, geom=False)
    with torch.no_grad():
        model(x)
    loss = model(x).mean()
    print(loss.item())
    loss.backward()  
    model.zero_grad()  

    model = capsules(E=20, geom=True)
    with torch.no_grad():
        model(x)

    x = torch.randn(2,1,32,32, requires_grad=True) * scale
    model = capsules(A=64, B=8, C= 16, D=16, E=5, geom=True)
    loss = model(x).mean()
    print(loss.item())
    loss.backward()  
    model.zero_grad()  

    x = torch.randn(2,1,32,32, requires_grad=True) *scale
    model = capsules(geom=True, algebraic_attn=True)
    with torch.no_grad():
        model(x)

    model = capsules(A=64, B=8, C= 16, D=16, E=5, geom=True, algebraic_attn=True)
    loss = model(x).mean()
    print(loss.item())
    loss.backward()  
    model.zero_grad()  
