class Args:
    batch_size = 128
    test_batch_size = 700
    test_intvl = 1
    epochs=10
    lr = 3e-3
    weight_decay=2e-7
    no_cuda=False
    seed=1
    log_interval = 10
    em_iters=2
    snapshot_folder='./snapshots'
    data_folder='./data'
    dataset='smallNORB'