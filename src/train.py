from __future__ import print_function
from absl import logging
import argparse
import os
import shutil
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from tqdm import tqdm
import math

from model import capsules
from loss import SpreadLoss
from datasets import smallNORB
from ranger import Ranger
from filelock import FileLock

from tensorboardX import SummaryWriter

if os.path.exists('logs'):
    shutil.rmtree('logs')
os.makedirs('logs')

# Training settings
parser = argparse.ArgumentParser(description='PyTorch Matrix-Capsules-EM')
parser.add_argument('--batch-size', type=int, default=128, metavar='N',
                    help='input batch size for training (default: 128)')
parser.add_argument('--test-batch-size', type=int, default=128, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--test-intvl', type=int, default=1, metavar='N',
                    help='test intvl (default: 1)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 10)')

# parser.add_argument('--lr', type=float, default=3e-3, metavar='LR', 
#                     help='learning rate (default: 0.01)') # moein - according to openreview
parser.add_argument('--lr', type=float, default=3e-2, metavar='LR', 
                    help='learning rate (default: 0.01)') # moein - according to openreview
parser.add_argument('--lr_decay_steps', type=int, default=50000, metavar='LR', 
                    help='') 
parser.add_argument('--lr_warmup_steps', type=int, default=5000, metavar='LR', 
                    help='') 
parser.add_argument('--lr_final_factor', type=float, default=1e-2, metavar='LR', 
                    help='') 

# parser.add_argument('--weight-decay', type=float, default=2e-7, metavar='WD',
#                     help='weight decay (default: 0)') # moein - according to openreview
parser.add_argument('--weight-decay', type=float, default=3e-3, metavar='WD',
                    help='weight decay (default: 0)') # moein - according to openreview

parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--em-iters', type=int, default=2, metavar='N',
                    help='iterations of EM Routing')
parser.add_argument('--snapshot-folder', type=str, default='./logs', metavar='SF',
                    help='where to store the snapshots')
parser.add_argument('--data-folder', type=str, default=os.path.expanduser('~/data/capsules'), metavar='DF',
                    help='where to store the datasets')
parser.add_argument('--dataset', type=str, default='smallNORB', metavar='D',
                    help='dataset for training(mnist, smallNORB)')
parser.add_argument('--debug', action='store_true', default=False)
parser.add_argument('--net-a', type=int, default=64, metavar='N', help='network architecture parameter')
parser.add_argument('--net-b', type=int, default=8, metavar='N', help='network architecture parameter')
parser.add_argument('--net-c', type=int, default=16, metavar='N', help='network architecture parameter')
parser.add_argument('--net-d', type=int, default=16, metavar='N', help='network architecture parameter')
parser.add_argument('--optimizer', type=str, default='ranger', metavar='O', help='optimizer (ranger|adam|sgd)')
parser.add_argument('--geom', type=int, default=0, metavar='G', help='use geometric variant')
parser.add_argument('--alg-attn', type=int, default=0, metavar='AA', help='carry out attention ops in the Lie algebra')
parser.add_argument('--exact-matexp', type=int, default=0, metavar='EM', help='use more accurate/matrix exponential')


args, best_prec1, swriter, global_step = None, None, None, 0

def get_setting(args):
    kwargs = {'num_workers': 4, 'pin_memory': True} if args.cuda else {}
    if not os.path.exists(args.data_folder):
        os.makedirs(args.data_folder)
    path = os.path.join(args.data_folder, args.dataset)
    with FileLock(os.path.join(args.data_folder, 'lock')):
        if args.dataset == 'mnist':
            num_class = 10
            train_loader = torch.utils.data.DataLoader(
                datasets.MNIST(path, train=True, download=True,
                            transform=transforms.Compose([
                                transforms.ToTensor(),
                                transforms.Normalize((0.1307,), (0.3081,))
                            ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)
            test_loader = torch.utils.data.DataLoader(
                datasets.MNIST(path, train=False,
                            transform=transforms.Compose([
                                transforms.ToTensor(),
                                transforms.Normalize((0.1307,), (0.3081,))
                            ])),
                batch_size=args.test_batch_size, shuffle=True, **kwargs)
        elif args.dataset == 'smallNORB':
            num_class = 5
            train_loader = torch.utils.data.DataLoader(
                smallNORB(path, train=True, download=True,
                        transform=transforms.Compose([
                            transforms.Resize(48),
                            transforms.RandomCrop(32),
                            transforms.ColorJitter(brightness=32./255, contrast=0.5),
                            transforms.ToTensor(),
                            transforms.Normalize([0.752], [0.177])
                        ])),
                batch_size=args.batch_size, shuffle=True, **kwargs)
            test_loader = torch.utils.data.DataLoader(
                smallNORB(path, train=False,
                        transform=transforms.Compose([
                            transforms.Resize(48),
                            transforms.CenterCrop(32),
                            transforms.ToTensor(),
                            transforms.Normalize([0.752], [0.177])
                        ])),
                batch_size=args.test_batch_size, shuffle=True, **kwargs)
        else:
            raise NameError('Undefined dataset {}'.format(args.dataset))
    return num_class, train_loader, test_loader


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    pred = output.max(-1)[1]
    correct = pred.eq(target.flatten()).float().mean()
    return [correct]

    # maxk = max(topk)
    # batch_size = target.size(0)

    # _, pred = output.topk(maxk, 1, True, True)
    # pred = pred.t()
    # correct = pred.eq(target.view(1, -1).expand_as(pred))

    # res = []
    # for k in topk:
        # correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
        # res.append(correct_k.mul_(100.0 / batch_size))
    # return res

def exp_lr_decay(optimizer, global_step, init_lr = 3e-3, decay_steps = 20000,
                                        decay_rate = 0.96, lr_clip = 3e-3 ,staircase=False):
    
    ''' decayed_learning_rate = learning_rate * decay_rate ^ (global_step / decay_steps)  '''
    
    if staircase:
        lr = (init_lr * decay_rate**(global_step // decay_steps)) 
    else:
        lr = (init_lr * decay_rate**(global_step / decay_steps)) 
    
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def cos_lr_decay(optimizer, global_step, base_lr = 3e-3, decay_steps = 20000, final_lr =3e-5, warmup_steps=5000):
    ''' decayed_learning_rate = learning_rate * decay_rate ^ (global_step / decay_steps)  '''
    
    if global_step < warmup_steps:
        lr = base_lr * global_step/warmup_steps
    elif global_step >= decay_steps:
        lr = final_lr
    else:
        lr = (base_lr - final_lr) * .5 * (1. + math.cos(
            math.pi * (global_step / decay_steps)
            )) + final_lr
    
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    return lr


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def train(train_loader, model, criterion, optimizer, epoch, device):
    global global_step
    batch_time = AverageMeter()
    data_time = AverageMeter()

    model.train()
    train_len = len(train_loader)
    epoch_acc = 0
    end = time.time()

    for batch_idx, (data, target) in enumerate(tqdm(train_loader)):
        data_time.update(time.time() - end)

        current_lr = cos_lr_decay(optimizer=optimizer, global_step=global_step, base_lr=args.lr, decay_steps=args.lr_decay_steps, final_lr=args.lr_final_factor * args.lr, warmup_steps=args.lr_warmup_steps)

        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        r = (1.*batch_idx + (epoch-1)*train_len) / (args.epochs*train_len)
        loss = criterion(output, target, r)
        acc = accuracy(output, target)

        # exp_lr_decay(optimizer = optimizer, global_step = global_step,
                # init_lr = args.lr,
                # decay_steps=args.lr_decay_steps,
                # decay_rate=args.lr_decay_rate,
                # staircase=args.lr_staircase,
                # ) # moein - change the learning rate exponentially


        loss.backward()
        optimizer.step()

        batch_time.update(time.time() - end)
        end = time.time()

        acc = acc[0].detach().cpu().item()
        loss = loss.detach().cpu().item()
        epoch_acc += acc
        if batch_idx % args.log_interval == 0:
            logging.info('Train Epoch: {}\t[{}/{} ({:.0f}%)]\t'
                  'Loss: {:.6f}\tAccuracy: {:.6f}\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})'.format(
                  epoch, batch_idx * len(data), len(train_loader.dataset),
                  100. * batch_idx / len(train_loader),
                  loss, acc,
                  batch_time=batch_time, data_time=data_time))
            swriter.add_scalar('train_loss', loss, global_step=global_step)
            swriter.add_scalar('train_acc', acc, global_step=global_step)
            swriter.add_scalar('train_batch_time', batch_time.val, global_step=global_step)
            swriter.add_scalar('train_data_time', data_time.val, global_step=global_step)
            swriter.add_scalar('learning_rate', current_lr, global_step=global_step)
        global_step += 1
        if args.debug:
            break
    return epoch_acc


def snapshot(model, folder, epoch, best_acc):
    path = os.path.join(folder, 'model_{}.pth'.format(epoch))
    path2 = os.path.join(folder, 'best_result.txt')
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    logging.info('saving model to {}'.format(path))
    torch.save(model.state_dict(), path)
    with open(path2, 'w') as f:
        for line in best_acc:
            f.write(str(line))


def test(test_loader, model, criterion, device):
    model.eval()
    test_loss = 0
    acc = 0
    test_len = len(test_loader)
    with torch.no_grad():
        for data, target in tqdm(test_loader):
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += criterion(output, target, r=1).detach().cpu().item()
            acc += accuracy(output, target)[0].detach().cpu().item()
            if args.debug:
                break

    test_loss /= test_len
    acc /= test_len
    logging.info('\nTest set: Average loss: {:.6f}, Accuracy: {:.6f} \n'.format(
        test_loss, acc))
    swriter.add_scalar('test_loss', test_loss, global_step=global_step)
    swriter.add_scalar('test_acc', acc, global_step=global_step)
    return acc


def main():
    global args, best_prec1, swriter
    args = parser.parse_args()
    logging.set_verbosity(logging.INFO)
    swriter = SummaryWriter(args.snapshot_folder)
    args.cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)

    device = torch.device("cuda" if args.cuda else "cpu")
    
    # datasets
    num_class, train_loader, test_loader = get_setting(args)

    # model
    A, B, C, D = args.net_a, args.net_b, args.net_c, args.net_d
    # A, B, C, D = 64, 8, 16, 16
    # A, B, C, D = 32, 32, 32, 32
    model = capsules(A=A, B=B, C=C, D=D, E=num_class,
                     iters=args.em_iters, geom=bool(args.geom),
                     algebraic_attn=bool(args.alg_attn),
                     closed_form= not bool(args.exact_matexp)
                     ).to(device)

    criterion = SpreadLoss(num_class=num_class, m_min=0.2, m_max=0.9)
    
    if args.optimizer == 'adam':
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    elif args.optimizer == 'ranger':
        optimizer = Ranger(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    elif args.optimizer == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=1. - args.weight_decay)
    else:
        raise ValueError('unknown optimizer: {}'.format(args.optimizer))

    test_accs = [test(test_loader, model, criterion, device)]
    for epoch in range(1, args.epochs + 1):
        acc = train(train_loader, model, criterion, optimizer, epoch, device)
        acc /= len(train_loader)
        if epoch % args.test_intvl == 0:
            test_accs.append(test(test_loader, model, criterion, device))
            snapshot(model, args.snapshot_folder, epoch, test_accs)
        if args.debug:
            break
    test_accs.append(test(test_loader, model, criterion, device))
    best_acc = max(test_accs)
    logging.info('best test accuracy: {:.6f}'.format(best_acc))
    swriter.add_scalar('best_acc', best_acc, global_step=global_step)

    snapshot(model, args.snapshot_folder, args.epochs, test_accs)
    swriter.close()

if __name__ == '__main__':
    main()
